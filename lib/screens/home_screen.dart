import 'dart:html';

import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});
  @override
  State<HomeScreen> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body: SafeArea(
        child: ListView(
          scrollDirection: Axis.vertical,
          padding: const EdgeInsets.all(8),
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    margin: EdgeInsets.all(8),
                    height: 50,
                    color: Colors.amber[200],
                    child: const Center(
                      child: Text(
                        'ประกาศเรื่อง',
                        style: TextStyle(fontSize: 25),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    margin: EdgeInsets.all(2),
                    padding: EdgeInsets.all(5),
                    child: Text(
                      '1.	การยื่นคำร้องขอสำเร็จการศึกษา ภาคปลาย ปีการศึกษา 2565',
                      style: TextStyle(
                          fontSize: 15,
                          decoration: TextDecoration.underline,
                          color: Colors.indigo.shade900),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    margin: EdgeInsets.all(8),
                    child: Image(
                        image: AssetImage('lib/screens/images/grad652.png')),
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    margin: EdgeInsets.all(2),
                    padding: EdgeInsets.all(5),
                    child: Text(
                      '2.	กำหนดการชำระค่าธรรมเนียมการลงทะเบียนเรียนภาคปลาย ปีการศึกษา 2565',
                      style: TextStyle(
                          fontSize: 15,
                          decoration: TextDecoration.underline,
                          color: Colors.indigo.shade900),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                    child: Container(
                  margin: EdgeInsets.all(8),
                  child:
                      Image(image: AssetImage('lib/screens/images/pay652.jpg')),
                )),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    margin: EdgeInsets.all(2),
                    padding: EdgeInsets.all(5),
                    child: Text(
                      '3.	แบบประเมินความคิดเห็นต่อการให้บริการของสำนักงานอธิการบดี',
                      style: TextStyle(
                          fontSize: 15,
                          decoration: TextDecoration.underline,
                          color: Colors.indigo.shade900),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                    child: Container(
                  margin: EdgeInsets.all(8),
                  child:
                      Image(image: AssetImage('lib/screens/images/632ass.jpg')),
                )),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
