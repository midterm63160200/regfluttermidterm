import 'dart:html';

import 'package:flutter/material.dart';


class HomeScreen extends StatefulWidget{
  const HomeScreen({super.key});
@override
  State<HomeScreen> createState() => _MyStatefulWidgetState();

}
class _MyStatefulWidgetState extends State<HomeScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
       child: ConstrainedBox(
        constraints: const BoxConstraints(maxWidth: 300.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            const Spacer(
              flex: 5,
            ),
            // const SizedBox(height: 50.0),
            const FlutterLogo(size: 100.0),
            const Spacer(
              flex: 10,
            ),

            //   const SizedBox(height: 100.0),
            const TextField(decoration: InputDecoration(labelText: 'Username')),
            const TextField(decoration: InputDecoration(labelText: 'Password')),
            const Spacer(
              flex: 3,
            ),

            // const SizedBox(height: 30.0),
            ElevatedButton(onPressed: () {}, child: const Text('Login')),
            const Spacer(
              flex: 20,
            ),
          ],
        ),
      ),
      ),
    );
  }
}
