import 'dart:html';
import 'grade_table.dart';
import 'package:flutter/material.dart';

class TranscriptScreen extends StatefulWidget {
  const TranscriptScreen({super.key});
  @override
  State<TranscriptScreen> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<TranscriptScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ListView(
            scrollDirection: Axis.vertical,
            padding: const EdgeInsets.all(8),
            children: <Widget>[
              Card(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.0),
                ),
                elevation: 8,
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      CircleAvatar(
                        radius: 50,
                        backgroundImage: NetworkImage(
                            "https://cdn-icons-png.flaticon.com/128/236/236832.png"),
                      ),
                      Text(
                        "ณัฐวุฒิ  จำปีคง ",
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "รหัสนิสิต: 63160000  \nคณะ: วิทยาการสารสนเทศ  สาขา: วิทยาการคอมพิวเตอร์",
                        style: TextStyle(fontSize: 13),
                      ),
                      Divider(),
                      Card(
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        color: Colors.amber[50],
                        child: Center(
                            child: Column(
                          children: [
                            Text(
                              "เกรดเฉลี่ย",
                              style: TextStyle(
                                  fontSize: 15,),
                            ),
                            Text(
                              "4.00",
                              style: TextStyle(
                                  fontSize: 40, fontWeight: FontWeight.bold),
                            ),
                          ],
                        )),
                      )
                    ],
                  ),
                ),
              ),
              Divider(),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      /* margin: EdgeInsets.all(1), */
                      padding: EdgeInsets.all(3),
                      child: Text(
                        'ปีการศึกษา: 1/2564',
                        style: TextStyle(fontSize: 15, color: Colors.black),
                      ),
                    ),
                  ),
                ],
              ),
              Divider(),
              Center(child: TableGrade164()),
              Divider(),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      /* margin: EdgeInsets.all(1), */
                      padding: EdgeInsets.all(3),
                      child: Text(
                        'ปีการศึกษา: 2/2564',
                        style: TextStyle(fontSize: 15, color: Colors.black),
                      ),
                    ),
                  ),
                ],
              ),
              Divider(),
              Center(child: TableGrade264()),
              Divider(),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      /* margin: EdgeInsets.all(1), */
                      padding: EdgeInsets.all(3),
                      child: Text(
                        'ปีการศึกษา: 1/2565',
                        style: TextStyle(fontSize: 15, color: Colors.black),
                      ),
                    ),
                  ),
                ],
              ),
              Divider(),
              Center(child: TableGrade165()),
              Divider(),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      /* margin: EdgeInsets.all(1), */
                      padding: EdgeInsets.all(3),
                      child: Text(
                        'ปีการศึกษา: 2/2565',
                        style: TextStyle(fontSize: 15, color: Colors.black),
                      ),
                    ),
                  ),
                ],
              ),
              Divider(),
              Center(child: TableGrade265()),
            ]),
      ),
    );
  }
}
