class Grage {
  final String code ;
  final String course;
  final String credit ;
  final String grade;


  Grage(this.code, this.course, this.credit,this.grade,);

  static List<Grage> getGrade164() {
    return [
      Grage('25710259', 'Economics of Everyday Life', '2','A'),
      Grage('30211359', 'Calculus', '3','A'),
      Grage(
          '88520159', '	Probability and Statistics for Computing', '3','A'),
      Grage(' 88520259', 'English for Informatics', '3','A'),
      Grage('88620159', 'Object-Oriented Programming Paradigm', '3','A'),
      Grage('88620259', 'Relational Database', '3','A'),
       Grage('99910159', 'English for Communication', '3','A'),
    ];
  }

   static List<Grage> getGrade264() {
    return [
      Grage('67521564', 'Financial and Fiscal Administration', '3','A'),
      Grage('88620359', 'Non-Relational Database', '3','A'),
      Grage(
          '88620459', '	Introduction to Data Science and Data Analytics', '3','A'),
      Grage('88621159', 'Data Structures and Algorithms', '3','A'),
      Grage('88622259', 'Operating Systems', '3','A'),
       Grage('88624159', 'Unix Tools and Programming', '3','A'),
    ];
  }
  static List<Grage> getGrade165() {
    return [
      Grage('88624259', 'Financial and Fiscal Administration', '3','A'),
      Grage('88631159', 'Non-Relational Database', '3','A'),
      Grage(
          '88633159', '	Introduction to Data Science and Data Analytics', '3','A'),
      Grage('88634159', 'Data Structures and Algorithms', '3','A'),
      Grage('88635359', 'Operating Systems', '3','A'),
       Grage('88636159', 'Unix Tools and Programming', '3','A'),
    ];
  }
  static List<Grage> getGrade265() {
    return [
      Grage('88624359', 'Web Programming', '3','A'),
      Grage('88624459', 'Object-Oriented Analysis and Design', '3','A'),
      Grage(
          '88624559', 'Software Testing', '3','A'),
      Grage('88634259', 'Multimedia Programming for Multiplatforms', '3','A'),
      Grage('88634459', 'Mobile Application Development I', '3','A'),
      Grage('88646259', 'Introduction to Natural Language Processing', '3','A'),
    ];
  }
}
