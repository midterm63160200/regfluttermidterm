import 'dart:html';

import 'package:flutter/material.dart';

class TableScreen extends StatefulWidget {
  const TableScreen({super.key});
  @override
  State<TableScreen> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<TableScreen> {
  @override
  Widget build(BuildContext context) {
    final ButtonStyle style = TextButton.styleFrom();
    return Scaffold(
        body: SafeArea(
            child: ListView(
                scrollDirection: Axis.vertical,
                padding: const EdgeInsets.all(8),
                children: <Widget>[
          Row(children: <Widget>[
            Expanded(
              child: Container(
                child: ButtonBar(
                  alignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    FilledButton(
                        style: const ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll<Color>(Colors.grey),
                        ),
                        onPressed: () {},
                        child: const Text(
                          '2/2565',
                          style: TextStyle(color: Colors.white),
                        )),
                    FilledButton(
                        style: const ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll<Color>(Colors.grey),
                        ),
                        onPressed: () {},
                        child: const Text(
                          '1/2565',
                          style: TextStyle(color: Colors.white),
                        )),
                    FilledButton(
                        style: const ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll<Color>(Colors.grey),
                        ),
                        onPressed: () {},
                        child: const Text(
                          '2/2564',
                          style: TextStyle(color: Colors.white),
                        )),
                    FilledButton(
                        style: const ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll<Color>(Colors.grey),
                        ),
                        onPressed: () {},
                        child: const Text(
                          '1/2564',
                          style: TextStyle(color: Colors.white),
                        )),
                  ],
                ),
              ),
            ),
          ]),

          Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    margin: EdgeInsets.all(8),
                    child: Image(
                        image: AssetImage('lib/screens/images/2.png')),
                  ),
                ),
              ],
            ),
          Row(children: <Widget>[
            Expanded(
              child: Container(
                child: ButtonBar(
                  alignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    TextButton(
                        onPressed: () {},
                        child: const Text(
                          'สอบกลางภาค',
                          style: TextStyle(color: Colors.blueGrey),
                        )),
                    TextButton(
                        onPressed: () {},
                        child: const Text(
                          'สอบปลายภาค',
                          style: TextStyle(color: Colors.blueGrey),
                        )),
                  ],
                ),
              ),
            )
          ]),
          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  color: Colors.grey,
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.all(5),
                  child: Text(
                    '27 มี.ค. 2566',
                    style: TextStyle(fontSize: 15, color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
          Card(
            child: ClipPath(
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  border: Border(
                    left: BorderSide(color: Colors.yellow, width: 20),
                  ),
                ),
                child: Text(
                  'รหัสวิชา 88624559-59 (3 หน่วยกิต)\nSoftware Testing การทดสอบซอฟต์แวร์\nเวลา 09:00-12:00 ห้อง IF-3M210',
                  style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
                ),
              ),
              clipper: ShapeBorderClipper(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(3))),
            ),
          ),
          Card(
            child: ClipPath(
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  border: Border(
                    left: BorderSide(color: Colors.yellow, width: 20),
                  ),
                ),
                child: Text(
                  'รหัสวิชา 88624359-59 (3 หน่วยกิต)\nWeb Programming การเขียนโปรแกรมบนเว็บ\nเวลา 17:00-20:00 ห้อง IF-4C01',
                  style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
                ),
              ),
              clipper: ShapeBorderClipper(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(3))),
            ),
          ),

          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  color: Colors.grey,
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.all(5),
                  child: Text(
                    '28 มี.ค. 2566',
                    style: TextStyle(fontSize: 15, color: Colors.white),
                  ),
                ),
              ),
            ],
          ),

          Card(
            child: ClipPath(
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  border: Border(
                    left: BorderSide(color: Colors.yellow, width: 20),
                  ),
                ),
                child: Text(
                  'รหัสวิชา 88624459-59 (3 หน่วยกิต)\nObject-Oriented Analysis and Design การวิเคราะห์และออกแบบเชิงวัตถุ\nเวลา 17:00-20:00 ห้อง IF-11M280',
                  style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
                ),
              ),
              clipper: ShapeBorderClipper(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(3))),
            ),
          ),

          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  color: Colors.grey,
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.all(5),
                  child: Text(
                    '29 มี.ค. 2566',
                    style: TextStyle(fontSize: 15, color: Colors.white),
                  ),
                ),
              ),
            ],
          ),

          Card(
            child: ClipPath(
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  border: Border(
                    left: BorderSide(color: Colors.yellow, width: 20),
                  ),
                ),
                child: Text(
                  'รหัสวิชา 88634459-59 (3 หน่วยกิต)\nMobile Application Development I การพัฒนาโปรแกรมประยุกต์บนอุปกรณ์เคลื่อนที่ 1\nเวลา 09:00-12:00 ห้อง IF-4C01',
                  style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
                ),
              ),
              clipper: ShapeBorderClipper(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(3))),
            ),
          ),

          Card(
            child: ClipPath(
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  border: Border(
                    left: BorderSide(color: Colors.yellow, width: 20),
                  ),
                ),
                child: Text(
                  'รหัสวิชา 88634259-59  (3 หน่วยกิต)\nMultimedia Programming for Multiplatforms การโปรแกรมสื่อผสมสำหรับหลายแพลตฟอร์ม\nเวลา 3:00-16:00 ห้อง IF-3M210',
                  style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
                ),
              ),
              clipper: ShapeBorderClipper(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(3))),
            ),
          ),

        
          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  color: Colors.grey,
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.all(5),
                  child: Text(
                    '31 มี.ค. 2566',
                    style: TextStyle(fontSize: 15, color: Colors.white),
                  ),
                ),
              ),
            ],
          ),

          Card(
            child: ClipPath(
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  border: Border(
                    left: BorderSide(color: Colors.yellow, width: 20),
                  ),
                ),
                child: Text(
                  'รหัสวิชา 88646259-59 (3 หน่วยกิต)\nIntroduction to Natural Language Processing การประมวลผลภาษาธรรมชาติเบื้องต้น\nเวลา 09:00-12:00 ห้อง IF-11M280',
                  style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
                ),
              ),
              clipper: ShapeBorderClipper(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(3))),
            ),
          ),
        ])));
  }
}
