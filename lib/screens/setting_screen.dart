import 'dart:html';

import 'package:flutter/material.dart';

class SettingScreen extends StatefulWidget {
  const SettingScreen({super.key});
  @override
  State<SettingScreen> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<SettingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: ListView(
        scrollDirection: Axis.vertical,
        padding: const EdgeInsets.all(8),
        children: <Widget>[
          Card(
            color: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
            ),
            elevation: 8,
            child: Container(
              margin: EdgeInsets.all(10),
              child: Column(
                children: [
                  CircleAvatar(
                    radius: 50,
                    backgroundImage: NetworkImage(
                        "https://cdn-icons-png.flaticon.com/128/236/236832.png"),
                  ),
                  SizedBox(height: 20),
                  Text(
                    "ณัฐวุฒิ  จำปีคง ",
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "รหัสนิสิต: 63160000  \nคณะ: วิทยาการสารสนเทศ  สาขา: วิทยาการคอมพิวเตอร์",
                    style: TextStyle(fontSize: 13),
                  ),
                  Divider(),
                  ElevatedButton.icon(
                    onPressed: () {},
                    label: Text("เเก้ไขประวัติส่วนตัว"),
                    icon: Icon(Icons.edit_square
                    ,color: Colors.blueGrey,),
                  )
                ],
              ),
            ),
          ),
          Divider(),
          ListTile(
            leading: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100), color: Colors.grey),
              child: Icon(
                Icons.settings,
                color: Colors.white,
              ),
            ),
            title: Text(
              "ตั้งค่า",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            trailing: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: Colors.grey.withOpacity(0.1)),
              child: Icon(
                Icons.keyboard_arrow_right,
                color: Colors.grey,
              ),
            ),
          ),
          Divider(),
          ListTile(
            leading: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100), color: Colors.grey),
              child: Icon(
                Icons.logout,
                color: Colors.white,
              ),
            ),
            title: Text(
              "ออกจากระบบ",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Divider(),
        ],
      )),
    );
  }
}
