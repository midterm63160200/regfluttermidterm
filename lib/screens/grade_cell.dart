import 'package:flutter/material.dart';

import 'grade.dart';

class GradeCell extends StatelessWidget {
  final String title;
  final bool isHeader;

  const GradeCell({
    Key? key,
    required this.title,
    this.isHeader = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: isHeader ? Alignment.center : Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Text(title,
            style: TextStyle(
                fontWeight: isHeader ? FontWeight.bold : FontWeight.normal)),
      ),
    );
  }
}
