import 'package:flutter/material.dart';

import 'grade_cell.dart';
import 'grade.dart';

class TableGrade164 extends StatelessWidget {
  const TableGrade164({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Table(
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: <TableRow>[
        TableRow(
          decoration: BoxDecoration(
            color: Colors.green[200],
          ),
          children: const <Widget>[
            GradeCell(title: 'รหัสวิชา', isHeader: true),
            GradeCell(title: 'ชื่อรายวิชา', isHeader: true),
            GradeCell(title: 'หน่วยกิต', isHeader: true),
            GradeCell(title: 'เกรด', isHeader: true),
          ],
        ),
        ...Grage.getGrade164().map((employee) {
          return TableRow(children: [
            GradeCell(title: employee.code),
            GradeCell(title: employee.course),
            GradeCell(title: employee.credit),
            GradeCell(title: employee.grade),
            
          ]);
        }),
      ],
    );
  }
}

class TableGrade264 extends StatelessWidget {
  const TableGrade264({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Table(
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: <TableRow>[
        TableRow(
          decoration: BoxDecoration(
            color: Colors.green[200],
          ),
          children: const <Widget>[
            GradeCell(title: 'รหัสวิชา', isHeader: true),
            GradeCell(title: 'ชื่อรายวิชา', isHeader: true),
            GradeCell(title: 'หน่วยกิต', isHeader: true),
            GradeCell(title: 'เกรด', isHeader: true),
          ],
        ),
        ...Grage.getGrade264().map((employee) {
          return TableRow(children: [
            GradeCell(title: employee.code),
            GradeCell(title: employee.course),
            GradeCell(title: employee.credit),
            GradeCell(title: employee.grade)
          ]);
        }),
      ],
    );
  }
}

class TableGrade165 extends StatelessWidget {
  const TableGrade165({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Table(
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: <TableRow>[
        TableRow(
          decoration: BoxDecoration(
            color: Colors.green[200],
          ),
          children: const <Widget>[
            GradeCell(title: 'รหัสวิชา', isHeader: true),
            GradeCell(title: 'ชื่อรายวิชา', isHeader: true),
            GradeCell(title: 'หน่วยกิต', isHeader: true),
            GradeCell(title: 'เกรด', isHeader: true),
          ],
        ),
        ...Grage.getGrade165().map((employee) {
          return TableRow(children: [
            GradeCell(title: employee.code),
            GradeCell(title: employee.course),
            GradeCell(title: employee.credit),
            GradeCell(title: employee.grade)
          ]);
        }),
      ],
    );
  }
}

class TableGrade265 extends StatelessWidget {
  const TableGrade265({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Table(
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: <TableRow>[
        TableRow(
          decoration: BoxDecoration(
            color: Colors.green[200],
          ),
          children: const <Widget>[
            GradeCell(title: 'รหัสวิชา', isHeader: true),
            GradeCell(title: 'ชื่อรายวิชา', isHeader: true),
            GradeCell(title: 'หน่วยกิต', isHeader: true),
            GradeCell(title: 'เกรด', isHeader: true),
          ],
        ),
        ...Grage.getGrade265().map((employee) {
          return TableRow(children: [
            GradeCell(title: employee.code),
            GradeCell(title: employee.course),
            GradeCell(title: employee.credit),
            GradeCell(title: employee.grade)
          ]);
        }),
      ],
    );
  }
}