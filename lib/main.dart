import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import './screens/home_screen.dart';
import './screens/table_screen.dart';
import './screens/setting_screen.dart';
import './screens/Transcript_screen.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  //int itemsInex =0;
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'RegFlutter',
        theme: ThemeData(
        primarySwatch: Colors.yellow,
        ),
        home: MyStatefulWidget()
    ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({super.key});

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0; // เปลี่ยนตรงนี้เป็น 0
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    HomeScreen(),
    TableScreen(),
    TranscriptScreen(),
    SettingScreen()


    
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ClipOval(
          child: Image(image: Image.network('https://cdn-icons-png.flaticon.com/128/236/236832.png').image,),
        ),
        
        
      ),
      title: const Text('มหาวิทยาลัยบูรพา'),
      actions: <Widget>[
        IconButton(onPressed: (){},icon: Icon(Icons.notifications_active,size:30,color: Colors.blueGrey,),)
      ],
      
      
      ),
      

      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home,color: Colors.blueGrey,size:30),
            label: 'หน้าหลัก',

            
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.calendar_month,color: Colors.blueGrey,size:30),
            label: 'ตารางเรียน/สอบ',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.school,color:Colors.blueGrey,size:30),
            label: 'ผลการศึกษา',
            
          ),
          
           BottomNavigationBarItem(
            icon: Icon(Icons.settings,color:Colors.blueGrey,size:30),
            label: 'ตั้งค่า',
           
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor:  Colors.blueGrey,
        onTap: _onItemTapped,
      ),
    );
  }
}
